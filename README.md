
# Rasc version 2
## Third year project report

The development of Rasc, between *Rasc version 1* and *Rasc version 2*
formed my third year project at Manchester University. For more information
about the Rasc project, see [my website](http://wilsonco.mooo.com/rasc)
or [the Rasc git repository](https://gitlab.com/wilsonco-moo/rasc).

As part of development of this project, I had to write a project report.
The final version of this, as a pdf file, is
[available here](https://gitlab.com/wilsonco-moo/rascreport/-/raw/master/rasc.pdf).

I also had to make a video, explaining and demonstrating the project:
this is [available here](https://youtu.be/t91tilMwm9c).
