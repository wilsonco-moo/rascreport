filename = rasc

pdfAndOpen:
	pkill -9 pdflatex || echo
	if [ -d build ]; then rm -r build/* || echo; fi
	mkdir build || echo
	cd build && ln -s `find .. -maxdepth 1 | grep -vE '^\.\.$$|^\.\./build$$|\.pdf$$'` . || echo
	cd build && pdflatex --shell-escape $(filename).tex
	cd build && pdflatex --shell-escape $(filename).tex
	cd build && pdflatex --shell-escape $(filename).tex
	mv build/$(filename).pdf $(filename).pdf
	rm -rf build || echo
	echo "Word count: `detex $(filename).tex | wc -w`"
	atril $(filename).pdf 2> /dev/null

defaultPdf:
	pkill -9 pdflatex || echo
	if [ -d build ]; then rm -r build/* || echo; fi
	mkdir build || echo
	cd build && ln -s `find .. -maxdepth 1 | grep -vE '^\.\.$$|^\.\./build$$|\.pdf$$'` . || echo
	cd build && pdflatex --shell-escape $(filename).tex
	cd build && pdflatex --shell-escape $(filename).tex
	cd build && pdflatex --shell-escape $(filename).tex
	mv build/$(filename).pdf $(filename).pdf
	rm -rf build || echo
	echo "Word count: `detex $(filename).tex | wc -w`"

zip:
	pkill -9 pdflatex || echo
	if [ -d build ]; then rm -r build/* || echo; fi
	mkdir build || echo
	cd build && ln -s `find .. -maxdepth 1 | grep -vE '^\.\.$$|^\.\./build$$|\.pdf$$'` . || echo
	cd build && pdflatex --shell-escape $(filename).tex
	cd build && pdflatex --shell-escape $(filename).tex
	cd build && pdflatex --shell-escape $(filename).tex
	mv build/$(filename).pdf $(filename).pdf
	rm build/*
	cp $(filename).pdf build/$(filename).pdf
	cd build && zip $(filename).zip $(filename).tex $(filename).pdf
	mv build/$(filename).zip .
	rm -rf build || echo
	echo "Word count: `detex $(filename).tex | wc -w`"

clear:
	rm -f $(filename).pdf
	rm -f $(filename).zip
	rm -rf build
