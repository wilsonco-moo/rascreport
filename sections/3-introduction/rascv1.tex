

\subsection{Rasc version 1}

In Rasc, the game is set on a \KEY{map} which is divided into \KEY{areas}
and subdivided into \KEY{provinces}. At the start of the game,
the user picks one of the available \KEY{maps}, each of which is
created using image textures and configuration files. By default,
Rasc comes with two \KEY{maps}:
\begin{noPageBreak}
\begin{labeling}{Iceland map:}
\item[Iceland map:]
    This is a small map, containing few \KEY{provinces}. It is helpful for
    learning the game, and for development as its textures load quickly.
\item[UK map:]
    This is a much larger map containing many more \KEY{provinces} and is
    being suitable for a longer duration game. The UK map is shown
    in \autoref{fig:rascv1}.
\end{labeling}
\end{noPageBreak}
Maps can be created at a very large \KEY{scale}, as the only limitations are
the hardware used to display the map, and the \mbox{time-consuming}
process of map design. In principle much larger maps than the \KEY{UK map}
could be produced, such as a map of the entire world.

\begin{figure}[ht]
    \centering
    \includegraphics[width=0.8\linewidth]{sections/3-introduction/rascv1.png}
    \caption{A screenshot from \KEY{Rasc version 1}, showing the UK \KEY{map}.}
    \label{fig:rascv1}
\end{figure}

Each \KEY{province} is either unowned, or owned by a particular
\KEY{player}. Ownership is displayed
using the colour associated with each \KEY{player}
(\autoref{fig:rascv1}). Additionally, only
\KEY{accessible} provinces can be claimed by \KEY{players} -- other
\KEY{provinces} (such as those in the sea) are \KEY{inaccessible}.
Seas can be crossed using shipping lanes, typically shown as lines
across the sea, (such as the one connecting South Wales and Ireland
in \autoref{fig:rascv1}).

For each \KEY{player}, their main objective is to claim as many
\KEY{provinces} as they can. \KEY{Provinces} are
grouped into \KEY{areas}, and when a player owns every
\KEY{province} in an \KEY{area}, they receive a bonus. This
provides a strategic objective to take entire \KEY{areas} at once.
In general, the victory condition in Rasc is to claim all
\KEY{accessible provinces} on the map.

\KEY{Players} are either controlled by a human or, in the case of AI players,
by the computer (\AUTOPAGEREF{ai}). Many (human and AI)
\KEY{players} can compete against each other on the same map,
at a much larger \KEY{scale} than is practical with a regular
board game such as Risk. Consequently, all parts of the game
(such as the system controlling AI \KEY{players}) must be
designed in a \KEY{scalable} way.
Due to the game's multiplayer functionality, these human \KEY{players}
do not have to be in the same location -- they can play over the internet.

The game is turn-based. This means that each \KEY{player} can
only do a certain set of actions in each \KEY{turn} and the
game cannot progress until all players have completed their
\KEY{turns}. Because a large number of human \KEY{players}
may be competing against each other, the game would take
unacceptably long if they had to wait for each human \KEY{player}
to complete their turns one after another. Because of this,
human \KEY{players'} turns are completed \KEY{simultaneously},
while AI \KEY{players'} turns are completed \KEY{sequentially},
making the turn system \KEY{partially simultaneous}.
This is described in more detail in \AUTOPAGEREF{turnSystem}.

To claim \KEY{provinces}, and to engage in combat with other players,
\KEY{armies} are used. \KEY{Armies}, which consist of a multiple
of \KEY{army units}, can be placed by \KEY{players}
in \KEY{provinces} and moved between adjacent \KEY{provinces}.
When armies owned by multiple opposing \KEY{players} are moved into
the same \KEY{province}, a \KEY{battle} takes place --
which in \KEY{Rasc version 1} are simple and happen instantly.
