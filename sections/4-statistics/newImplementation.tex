
\subsection{Improved implementation of the \KEY{statistics system}}\label{newStatSystem}

To address the performance issues, the \KEY{statistics system} had to be
re-designed. Instead of generating data from single, monolithic update
operations, it is now \KEY{event-driven}\cite{eventDriven}.

Initially, when the map is loaded, the \KEY{statistics system} generates all
the data in a similar way to its old update operation -- this provides
initial data values. Then, whenever something in the map is changed (such as a
province changing ownership), the \emph{statistics system} is notified immediately.
In response, all affected data is updated to take this
into account, in a (usually) constant time operation\footnote{
    All update operations are constant time, except the system which
    maintains a set of pairs of adjacent players, \textbf{sorted} by
    similarity of player colours. Due to maintaining a sorted data
    structure, this takes \emph{logarithmic time}. Note that updates
    to this are rare, as it is only updated when players gain or lose
    adjacencies to other players, rather then when players gain or lose
    provinces.
}, while unaffected data is retained.

This means that every time something changes in the map, the
\KEY{statistics system} remains \KEYB{always up-to-date}. This is
especially important for AI players, as it means they always have
correct data, without requiring a slow and costly \KEY{statistics} update
operation before processing their turn. The \KEY{statistics system} is
notified about the following changes:
\begin{itemize}
    \item Changes in province ownership, in terms of old owner and new owner.
          This is the most important operation, as it affects most
          \KEY{statistics}. For example, this updates player \KEY{resources}
          (\KEY{manpower} and \KEY{currency}), by subtracting the province's
          \KEY{resource} output from the old player and adding it to the
          new player.
    \item Changes to the number of standing armies in a province and who
          owns them. This is triggered whenever an army is moved, placed,
          or when a battle/siege phase takes place. This updates the
          locations and counts of deployed armies per player.
    \item Changes to \KEY{resource} output in any province: this is
          triggered when players construct or demolish buildings.
          The change in \KEY{resource} output is added onto the province
          owner's per-turn \KEY{resources}, (\KEY{manpower} and
          \KEY{currency} per turn). Special care must be taken when
          players own entire areas -- the 50\% \KEY{resource} bonus
          must remain consistent.
    \item Addition and removal of players: addition of players is used to
          create data structures to store their data. When players are removed,
          the \KEY{statistics system} checks whether they are \KEY{allowed}
          to be removed -- a player who owns armies or provinces cannot be
          removed from the game.
    \item Changes to player colours: this updates the set of player colour
          pairs, which is used for avoiding adjacent similar colours
          (\AUTOPAGEREF{colours}), to improve accessibility.
\end{itemize}
Implementing this greatly reduced turn delays, and improved \KEY{scalability}
as the turn system now takes approximately linear ($O(n)$) time. On a
medium-sized map, turn delays have reduced from multiple seconds, to tens of
milliseconds\footnote{\label{newStatsSpeedFootnote}
    Tested on Rasc version 1.0942, compiled in development mode
    (optimisation disabled).
    Turn delay reduced from 2151.2 to 39.1 milliseconds, averaged over the
    first 10 turns, (ignoring the first turn which has a larger delay due to
    setup). This was tested using the same map as
    \AUTOPAGEREF{turnDelayFootnote}, also using the hardware described
    in \mbox{this reference\cite{hardware}.}
}.

\subsection{Summary}

The addition of \KEY{resources} in Rasc make for some interesting new
\KEY{game mechanics}. Implementing this concept required a
system able to cope with Rasc's large \KEY{scale}, while
always keeping information \mbox{up-to-date} as the map changes.
Details such as \KEY{resources} still need to be displayed to users,
this is described in the next section.
