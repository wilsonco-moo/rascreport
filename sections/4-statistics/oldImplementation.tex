
\subsection{Initial implementation of the \KEY{statistics system}}

Players in Rasc only need to think strategically, as the considerable about
of `accounting' required to calculate \KEY{resources} and other
\KEY{statistics} in Rasc is performed automatically by the computer.
This creates a problem: the game is collecting
\KEY{statistics} from a map which changes as the game progresses. As
a result, these \KEY{statistics} need to be updated throughout the
game rather than being calculated once at the start. Due to Rasc's \KEY{scale},
this computation can be significant and expensive.

Alongside \KEY{manpower} and \KEY{currency}, \KEY{Rasc version 2}
additionally requires the following other \KEY{statistics}
also affected by map changes:
\sloppypar
\begin{itemize}
    \item Pairs of adjacent players, how many borders each pair of
          adjacent players share, and their colours. This is required for
          avoiding adjacent similar colours (\AUTOPAGEREF{colours}),
          and to enable future development of a diplomacy system.
    \item Counts of owned provinces and areas per player.
          These are required for the \KEY{list of player statistics} in the
          user interface (\AUTOPAGEREF{fig:wholeUI}), and are used by
          AI players for making decisions about \KEY{threat}
          (\AUTOPAGEREF{threat}) and \KEY{desirability}
          (\AUTOPAGEREF{desirability}).
    \item Lists of provinces for each player, categorised as shown in
          \AUTOPAGEREF{fig:irelandNation}. This is used extensively by
          AI players.
    \item The total number of armies which each player currently owns,
          and which provinces they are contained within. This is required
          for working out army maintenance costs for each player,
          and is required by AI players to plan how many armies to
          place, without over-spending.
\end{itemize}
The simplest way to keep \KEY{statistics} like this \mbox{up-to-date},
is by starting \mbox{`from scratch'} and iterating through all appropriate
data (provinces in this case), then repeating the entire process
at appropriate intervals. This takes time \KEYB{proportional}
to the \KEYB{size of the data} (number of provinces), each time the
\KEY{statistics} are updated.

This is how the \KEY{statistics system} was originally designed
in \KEY{Rasc version 1}, to provide data for the
\KEY{list of player statistics} (\AUTOPAGEREF{fig:wholeUI}).
The only collected \KEY{statistics} were the counts of owned provinces and
areas per player -- this worked fine as \KEY{statistics}
only needed updating \KEYB{once per turn}.

\subsection{Rasc's turn system (for context)}\label{turnSystem}

Rasc is a multiplayer turn-based game, where many human players can
compete against many AI players. In board games such as \KEY{Risk}\cite{risk},
it is typical for players to take their turns one after another, in a
\KEY{sequential} fashion.

Whilst this technique is definitely \KEY{possible} in a multiplayer
computer game, it does not \KEY{scale} well. It is analogous to a
\KEY{single threaded}\cite{thread} system, where the overall time taken is the
sum of durations of all tasks. As the number of human players increases,
each player has to wait increasingly long for all other players to
complete their turns -- resulting in a slow, dull game and players
losing interest.

A more \KEY{scalable} technique would be to perform the turns of all
human players \KEY{simultaneously}. This would be analogous to a
\KEY{multi-threaded}\cite{parallel} system with a synchronisation
barrier\cite{barrier}, where the overall time taken is reduced to that
of the longest task. Synchronisation is required to ensure that the game
does not progress until all human players have completed their turns.
In a multiplayer game this would allow many human players to compete without
excessive waiting.

To implement \KEY{simultaneous} turns for human players, two approaches
are possible. In the first approach every player performs actions
\KEY{simultaneously}, then `commits' what they have done. Players cannot
see what others have done until the results of all players'
actions are merged at the end. While this is a fair approach it can
be unintuitive for human players -- especially for those who are
used to games with \KEY{sequential} turns. Also, some of the game's
multiplayer aspects are `lost': human players cannot
interact with each other as easily, as they cannot see each others'
actions as they are taken.

The second approach (\autoref{fig:turnDiagram}) is a compromise between
\KEY{sequential} and \KEY{simultaneous} turn systems: the
\KEY{partially simultaneous} turn system used by both Rasc and
\KEY{Sid Meier's Civilization V}\cite{civ}\cite{civ5turns}. 
This works by \KEYB{first} running the turns for all AI players
\KEY{sequentially}, \KEYB{then} letting all human players take their
turns \KEY{simultaneously}, by all modifying a single \KEYB{shared map state}
-- this is analogous to a \KEY{shared memory}\cite{sharedMemory} system,
where the same memory can be modified by many threads simultaneously.

\KEY{Partially simultaneous} turns mean that all human players can see
and respond to the actions of other human players, \KEYB{as they happen} --
providing an engaging and intuitive multiplayer experience.
It also means that whenever human players are not directly interacting
(such as singleplayer mode, or whenever human players interact with AI players),
the turn system acts functionally identically to a simple \KEY{sequential}
turn system.

While this provides an engaging multiplayer
experience, it adds certain complications. Firstly, AI players' sequential
turns \KEYB{must execute quickly}, as human players have to wait for them.
Another consequence is that it complicates the fair implementation of
combat (\AUTOPAGEREF{battles}), since some players' turns are completed
\KEY{sequentially}, while others are completed \KEY{simultaneously}.

\begin{figure}[!ht]
    \centering
    \includegraphics[width=0.85\linewidth]{sections/4-statistics/turnDiagram.pdf}
    \caption{The \KEY{partially simultaneous} turn system used in
             \KEY{Sid Meier's Civilization V}\cite{civ}\cite{civ5turns}
             and Rasc.}
    \label{fig:turnDiagram}
\end{figure}

\subsection{Scalability of the \KEY{statistics system} in \KEY{Rasc version 2}}

\sloppypar
Where simply re-calculating all \KEY{statistics} each turn worked fine
for \KEY{Rasc version 1}, in \KEY{Rasc version 2} the requirements changed.
To facilitate new features, (such as \KEY{resources}), and to provide the
necessary  \KEYB{up-to-date} information for the new AI system
(see \AUTOPAGEREF{ai}), the \KEY{statistics system} had to be updated
immediately before the turns of \KEYB{each AI player}. Since
updating the \KEY{statistics system} takes time proportional to the
number of provinces in the map, and \KEYB{all} provinces can contain
a separate AI player at the start of the game, this becomes a problem with
a time complexity\cite{timeComplexity} of $O(n^2)$, (where $n$ is the number
of provinces in the map).

Because a map in Rasc can hold hundreds of provinces,
and the turns of AI players are completed sequentially
\KEYB{while human players are waiting}, this causes \emph{unacceptably long}
turn delays, in the order of multiple seconds\footnote{
    \label{turnDelayFootnote}
    Tested on Rasc version 1.0939, compiled in development mode
    (optimisation disabled).
    Average turn delay of 2151.2~ms. This is averaged over
    the first 10 turns of a game, on a map with 247 accessible provinces,
    (the UK map), all but one filled with AI players. This would be
    \textbf{much} slower on a larger map. The first turn (which has a larger
    delay due to setup), was ignored. Tested using the hardware described
    in \mbox{this reference\cite{hardware}.}
} on a medium-sized map. Of this, the vast majority of the time is spent
updating the \KEY{statistics system}, rather than processing the AI players'
turns -- this is certainly not ideal.
