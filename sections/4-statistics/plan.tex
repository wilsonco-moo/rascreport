\subsection{Motivation for new features}

In \KEY{Rasc version 1}, each player is able to place exactly
three \KEY{army units} per turn, and an additional three for each
area that they own completely (an area is owned completely
when the player owns every province within it). This is not a
flexible approach, and has the following problems:

\begin{itemize}
\item The \KEYB{only} way for a player to place more than three
      \KEY{army units} per turn is to claim a complete area.
      After doing that, the player suddenly jumps from three
      to six \KEY{army units} per turn. This can unbalance the game, as
      they are able to place \KEYB{double} the number of \KEY{army units}
      per turn as players around them.
\item Because ownership of complete areas is all that matters,
      all provinces appear the same to players. If each province
      was different, it would provide more opportunity for \KEY{strategy}, 
      as players would have an incentive for players to try to take
      more useful provinces.
\item \KEY{Army units} cannot be `reserved' to use later. If a player
      does not place the maximum number of \KEY{army units} in a turn,
      these \KEY{army units} are lost, and the player is at a
      disadvantage.
\end{itemize}

\subsection{Terrain type and terrain traits}

In \KEY{Rasc version 2}, provinces are not all the same -- they
each have characteristics. Each province now has\footnote{
    Note that even in \KEY{Rasc version 1} all provinces had a
    \KEY{terrain type} and some \KEY{terrain traits}. However,
    these were not visible to users and not used in
    the game -- they were a feature of the \KEY{map system},
    designed as a placeholder for future use. In
    \KEY{Rasc version 2} these features have been
    built upon and improved significantly, becoming visible
    to the user once the \KEY{province interface}
    (\AUTOPAGEREF{uiProvinceMenu}) and \KEY{resources}
    (\autoref{resources}) were added.
} a \KEY{terrain type} (such as plains, tundra or city), and some
\KEY{terrain traits} (such as hilly, mountainous and forested).
These are all shown in the \KEY{province interface}
(\AUTOPAGEREF{uiProvinceMenu}).

\KEY{Terrain traits} are important as they
characterise what a province `contains' and can be changed while the
game is running. Because of this, \KEY{terrain traits} are also used to
implement other game features such as \KEY{buildings}, \KEY{forts} and
\KEY{garrisons}.

\subsection{Resources}\label{resources}

Since Rasc is a war game based around \KEY{armies}, the two main factors
influencing how `powerful' a player is, are their abilities to \KEYB{place}
and \KEYB{maintain} \KEY{armies}. Intuitively, these should depend
on the characteristics of the provinces which that player owns.
For this to happen, \KEY{Rasc version 2} adds the idea of \KEY{resources}:
\KEY{manpower} and \KEY{currency}.

\begin{noPageBreak}
\begin{labeling}{Manpower:}
\item[Manpower:]
    This \KEY{resource} represents the total number of \KEY{army units} that
    a player has available for use in \KEY{armies}. Placing \KEY{army units}
    requires an \KEYB{upfront cost} in \KEY{manpower} -- once this has run out
    a player can no longer place \KEY{army units}. Placement of \KEYB{one}
    \KEY{army unit} requires \KEYB{one} \KEY{manpower}.
\item[Currency:]
    This \KEY{resource} represents the total
    \KEY{wealth} that a nation has. Each active \KEY{army unit} that a player
    owns has a \textbf{maintenance cost} of \KEY{currency} each turn.
    The more currency that a nation is generating \KEY{each turn}, the
    more active \KEY{army units} they can support. If a player spends more
    \KEY{currency} than they have, they go into debt. Once a
    player is in debt, they can no longer place new \KEY{army units} or
    construct \KEY{buildings}.
\end{labeling}
\end{noPageBreak}

\KEY{Resources} are gained each turn from the provinces that a
player owns. The amount of \KEY{resources} that a given province
generates each turn is dependent on the \KEY{terrain type} and
\KEY{terrain traits} within it. Information about the \KEY{resource}
output of each province is shown in the \KEY{province interface}
(\AUTOPAGEREF{uiProvinceMenu}).
All of this makes some provinces on the map more useful than
others, providing players an incentive to try to take the provinces
which best provide the \KEY{resources} they need most.

Players can also choose to improve their own provinces, by spending
\KEY{currency} to construct \KEY{buildings}. Different buildings
improve the province in different ways, for example a bank improves
\KEY{currency} output, and a barracks improves \KEY{manpower} output.

To facilitate this new \KEY{resource} system and the battle system
described in \AUTOPAGEREF{battles}, the scale of armies has been
changed (\autoref{fig:unitComparison}). Instead of clicking once to
place a single \KEY{army unit} (\KEY{Rasc version 1}), a human
player can place an army with a size of 1000 \KEY{army units},
by clicking once (\KEY{Rasc version 2}).

\begin{figure}[ht]
    \centering
    \includegraphics[width=0.4\linewidth]{sections/4-statistics/unitComparison.pdf}
    \caption{The smallest size of \KEY{army} that can be placed by a player:
             one \KEY{army unit} in \KEY{Rasc version 1} (left),
             and an army consisting of 1000 \KEY{army units} in
             \KEY{Rasc version 2} (right).}
    \label{fig:unitComparison}
\end{figure}
