\newpage

\section{User interface additions} \label{ui}

\begin{noPageBreakPar}
As new features have been added to Rasc, additional supporting user interfaces
were required. Each has been designed using Rasc's UI system\cite{rascui},
which provides functionality for layout, themes and consistent UI components.
\end{noPageBreakPar}

\subsection{Province interface} \label{uiProvinceMenu}

\begin{noPageBreakPar}
As was described in \AUTOPAGEREF{statistics}, in \KEY{Rasc version 2}
each province now has some associated characteristics. 
Each province has a \KEY{terrain type} and some
\KEY{terrain traits}, all of which contribute to the
\KEY{resources} that the province produces.
To provide human players with easy access to this information
the \KEY{province interface} has been added; this is opened whenever a
user clicks on a province.
\end{noPageBreakPar}

\begin{noPageBreakPar}
When designing the \KEY{province interface}, and most other parts of Rasc's
user interface, they were drawn first on paper. Various design iterations were
considered before the final design was chosen. This process is shown
(for the \KEY{province interface}), in
\autoref{fig:provinceMenuDesignProgression}.
\end{noPageBreakPar}

\begin{figure}[ht]
    \centering
    \includegraphics[width=\linewidth]{sections/5-ui/provinceMenuDesignProgression.png}
    \caption{Design progression for the \KEY{province interface}.}
    \label{fig:provinceMenuDesignProgression}
\end{figure}

\clearpage
\begin{noPageBreakPar}
The final design for the \KEY{province interface} is shown in
\autoref{fig:provinceMenu}.
The top-left section shows a pictorial
representation of the \KEY{terrain type} and \KEY{terrain traits},
allowing users to easily recognise them. A list of \KEY{terrain traits} and
\KEY{buildings} present in the province are shown at the bottom left.
On the right, details such as the \KEY{resource} output of the province
are shown, alongside ownership information. At the bottom are controls
for the \KEY{garrison} system.
\end{noPageBreakPar}

\begin{figure}[ht!]
    \centering
    \includegraphics[width=0.7\linewidth]{sections/5-ui/finalProvinceMenu.png}
    \caption{The final design for the \KEY{province interface}.}
    \label{fig:provinceMenu}
\end{figure}

\subsection{Displaying player information}\label{universalPlayerButton}

For each player, Rasc keeps track of the following associated information: 
\begin{itemize}
\item Counts of owned provinces and areas.
\item \KEY{Resources} such as \KEY{manpower} and \KEY{currency}
      (see \AUTOPAGEREF{resourceCounting}), generated from all of the
      provinces which the player owns.
\item For AI players, there are associated \KEY{behaviour values}
      which represent their behavioural characteristics,
      (see \AUTOPAGEREF{aiBehaviour}).
\end{itemize}
This kind of information is helpful for both AI and human players
to access. For example, AI players use the total province count
as part of the decision for how `threatening' another player seems
(\AUTOPAGEREF{threat}). Also, a human player could make
the decision to be more wary of an AI player that is shown as
being `very aggressive'.

\begin{noPageBreakPar}
As is shown in \autoref{fig:wholeUI}, new interfaces have been
added to display this information. The \KEY{list of player statistics}
shows a list of all players, each annotated with relevant
information. The list can be sorted by each category, allowing a human
player to work out how they are doing in comparison to other
players. Another new interface -- the \KEY{player information summary},
shows a summary of all useful information about a selected player.
\end{noPageBreakPar}

\begin{figure}[ht]
    \centering
    \includegraphics[width=\linewidth]{sections/5-ui/universalPlayerButton.pdf}
    \caption{How major parts of the user interface are linked together.}
    \label{fig:wholeUI}
\end{figure}

\begin{noPageBreakPar}
At various points within these new interfaces, players need to be
represented. For example, each province on the \KEY{map} has an
owner player, which is displayed in the \KEY{province interface}.
Players are also shown in the \KEY{list of player statistics}, and
the \KEY{player list} (see \autoref{fig:wholeUI}).
\end{noPageBreakPar}

\begin{noPageBreak}
To provide a consistent representation of a player,
the \KEY{universal player button} has been added.
This displays both the player's name and colour, helping the player
to be more easily identified. When the button is pressed, it leads to the
\KEY{player information summary}, focused on the relevant player.
This is helpful for navigating Rasc's user interface,
because wherever a player is shown, information about them is readily
available just by clicking.
\end{noPageBreak}

\begin{noPageBreakPar}
\subsection{Summary}

These new interfaces provide users with easy access
to information about players and provinces. Additionally, each user
interface is connected together in a way which aids navigation.
All of this exposes the new \KEY{game mechanics} in \KEY{Rasc version 2},
in a way which aims to be simple and intuitive for users.
\end{noPageBreakPar}
