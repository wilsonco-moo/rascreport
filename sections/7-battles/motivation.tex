\subsection{Design for new battles}

\KEY{Battles} between a player's armies and armies owned by an \KEY{enemy}
player are a fundamental concept in war games. In \KEY{Rasc version 1},
\KEY{battles} are simple: when a player moves their army into an \KEY{enemy}
province, that player's army and any present \KEY{enemy} armies take equal
losses, until only one army remains. If the player's army outnumbered the enemy
army (so they \KEY{won} the \KEY{battle}), the province ownership changes
to that player -- they have \KEY{taken} the province.

During a single turn, a player can wipe out
entire \KEY{enemy} armies, and take \KEY{enemy} provinces -- all without
the \KEY{enemy} player being able to defend. This is part of what makes
\KEY{Rasc version 1} such a fast-paced game, as border provinces tend to
change ownership almost every turn.

Part of the objectives of \KEY{Rasc version 2} are to make the game
more engaging, and to provide players with more options. To do this
with Rasc's combat system, inspiration was taken from land combat
in \KEY{Europa Universalis IV}\cite{eu4}\cite{eu4LandWarfare},
where \KEY{battles} happen slowly over time. In Rasc, this means that
when a player moves an army into a province containing an \KEY{enemy}
army, a \KEY{battle} starts, which progresses over multiple turns
(\autoref{fig:battle}).
This gives both players the chance to respond to the \KEY{battle},
by either retreating or reinforcing their armies.

An important point is that the \KEY{battle} system must be
\KEYB{fair for all players}, even with Rasc's \KEY{partially simultaneous}
turn system (\AUTOPAGEREF{turnSystem}). This means that the \KEY{battle} system
must not provide an exploitable advantage or disadvantage to \KEYB{any} player
(even AI players), regardless of the order in which turns take
place\footnote{
    Even with the \KEY{battle} behaviour described in
    \AUTOPAGEREF{scheduledBattles}, due to the
    \KEY{partially simultaneous} turn system, this is not
    \emph{quite} true. However, this is only a small inconsistency
    affecting the \emph{timing} of \KEY{battle phases} for the attacker
    under certain circumstances -- it is neither an advantage nor a
    disadvantage, so can still be considered \textbf{fair}.
}.

\subsection{Additional game mechanics: \KEY{sieges} and \KEY{garrisons}}\label{garrisonsSieges}

\KEY{Sieges} have also been added to Rasc, so that
\KEY{taking} provinces is no longer instant. A \KEY{siege} takes place
whenever a player's army is in an \KEY{enemy} province, without
an \KEY{enemy} army present to defend. Currently, \KEY{sieges} in
\KEY{Rasc version 2} take four turns, each turn the player's army
loses 1000 men. This gives the \KEY{enemy} player time to respond,
by moving in an army to defend before the province is taken.

\KEY{Rasc version 2} also adds the idea of \KEY{garrisons}. A \KEY{garrison}
is a special kind of army only usable to defend provinces from enemies.
\KEY{Garrisons} are stronger and cheaper to maintain than a normal army,
but \KEYB{cannot be moved}, so cannot be used to attack. 
A \KEY{garrisoned} army defends the province whenever another
player tries to \KEY{besiege} it.

For simplicity, \KEY{sieges} and \KEY{battles with garrisons} are implemented
using the same system as \KEY{battles}: a \KEY{battle} becomes a \KEY{siege}
once no defending army remains.

\begin{figure}[ht]
    \centering
    \includegraphics[width=0.7\linewidth]{sections/7-battles/battle.png}
    \caption{A \KEY{battle} in \KEY{Rasc version 2} is shown by both armies
             being present in the province at once. The army on the left (red)
             has 2,260 men, and the army on the right (dark blue) has
             13,100 men. Since \KEY{battles} are (currently) deterministic,
             the army on the left will lose this \KEY{battle} unless it
             is reinforced.}
    \label{fig:battle}
\end{figure}
