As is shown in \autoref{fig:singleBattlePhaseRetreatProblem}, if the
\emph{defender player} takes their turn \textbf{before} they are attacked by
the \emph{attacker player}, then a battle phase happens before the first
opportunity for the \emph{defender player} to retreat.

However, if the \emph{defender player} takes their turn \textbf{after}
they are attacked by the \emph{attacker player}, then they can retreat
immediately - before a battle phase has happened.

The \emph{defender player} always takes damage in the first case, but can
avoid damage in the second case. This means that the retreat behaviour
is not \emph{turn consistent}, as different turn orders result in different
outcomes. This system is also not ideal, as \emph{nothing} happens
immediately, which may seem counterintuitive to players.



\pagebreak
\subsection{Simple battle scheduling}

To help to solve this, rather than running a battle phase \textbf{once}
at the end of the turn, the idea of scheduling can be introduced.
For example, battle phases could be \emph{scheduled} to happen
\textbf{after} the \emph{attacker player} has completed their
turn, regardless of the turn order.

\begin{figure}[ht]
    \centering
    \includegraphics[width=\linewidth]{sections/6-battles/simpleBattleScheduling.pdf}
    \caption{Alternating the turn order, with simple
             battle \emph{scheduling}.}
    \label{fig:simpleBattlePhasing}
\end{figure}

As is shown in \autoref{fig:simpleBattlePhasing}, in the case of sequential
turns, the problem is solved. At the first opportunity for the
defender to retreat, \textbf{one} battle phase has always happened, regardless
of the turn order.

However, now consider the case of two human players
attacking each other: in Rasc's turn order (\AUTOPAGEREF{fig:turnDiagram})
human players take their turns \textbf{simultaneously}:

\begin{figure}[ht]
    \centering
    \includegraphics[width=\linewidth]{sections/6-battles/simultaneousProblem.pdf}
    \caption{Simple \emph{scheduling}, with simultaneous turns - such as two
             human players.}
    \label{fig:simultaneousProblem}
\end{figure}

Because the battle phase is \emph{scheduled} to happen after the
\emph{attacker} player's turn has completed, when turns happen simultaneously,
the \emph{defender player} can always retreat \textbf{before} it has happened.
This is no longer \emph{turn consistent}, as different turn orders result
in different outcomes.
