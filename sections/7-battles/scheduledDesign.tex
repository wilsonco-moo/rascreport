
\subsection{Introducing multiple different \KEY{types} of battle phase}\label{scheduledBattles}

To figure out a fairer approach for battles in Rasc, inspiration
was taken from how combat works in a
\KEY{Sid Meier's Civilization V}\cite{civ}\cite{civ5Combat}, an example
of which is shown in \autoref{fig:civ5CombatScreenshot}.
Unlike Rasc, in \KEY{Sid Meier's Civilization V} there are no battles,
but combat is still done in steps each turn, while remaining reasonably
\mbox{fair for all players}. Combat occurs by both players
\KEYB{alternately choosing} to attack each other, \KEYB{during} their
turns. Each attack deals damage that is larger for \mbox{`less damaged'}
armies.

\begin{figure}[ht]
    \centering
    \includegraphics[width=0.7\linewidth]{sections/7-battles/civ5combat/scr3.jpg}
    \caption{An example of combat between two armies (the army at the
             top is attacking the army at the bottom), in
             \KEY{Sid Meier's Civilization V}\cite{civ}\cite{civ5Combat}.}
    \label{fig:civ5CombatScreenshot}
\end{figure}

Since both Rasc and \KEY{Sid Meier's Civilization V} share the same
\KEY{partially simultaneous} turn system, it would seem that an approach
similar to this would be sensible to use in Rasc's battles\footnote{
    There are likely many other ways to approach this fairness problem, but this
    scheduling method seemed most sensible to me.
}. To do this, instead of Rasc having a \KEY{single type} of battle phase,
\KEY{multiple different types} of battle phase (listed below) have been
introduced, which each happen at different times during the turns.
By keeping track of which armies are \KEY{attacking} and \KEY{defending},
and \KEY{scheduling} these phases to happen at
appropriate times during the turns, Rasc's battle system is be made
\mbox{fair for all players}.

\begin{noPageBreak}
\begin{labeling}{Defence phase:}
\item[Strike phase:]
    The first part of a battle, happening \KEY{immediately}, as soon
    as the \KEY{attacker} moves their army in. This deals damage to
    \KEYB{both} armies, (but damages the \KEY{attacking} army more since
    a design decision was taken that \KEY{defending} armies should have
    an `upper hand'). For users, it is important that at least some part
    of the battle happens \KEY{immediately}, as it provides feedback
    that something is happening.
    
\item[Defence phase:]
    When the \KEY{defending} army responds by dealing damage to
    \KEY{attacking} army. The \KEY{attacking} army takes damage
    proportional to the size of the \KEY{defending} army.

\item[Attack phase:]
    When the \KEY{attacking} army attacks the \KEY{defending} army.
    The \KEY{defending} army takes damage proportional to the size of
    the \KEY{attacking} army.
\end{labeling}
\end{noPageBreak}

The order in which these battle phases are executed, is shown in
\autoref{fig:battlePhaseProcessDiagram}. The \KEY{strike phase}
is executed first (\KEY{immediately}). Next, after the \KEY{defender}'s turn,
the \KEY{defence phase} is executed. After the \KEY{attacker}'s next turn,
the \KEY{attack phase} is executed. This cycle is then repeated, simulating
the \KEYB{alternate} attack and defence style of combat, used in
\KEY{Sid Meier's Civilization V}.

\begin{figure}[ht]
    \centering
    \includegraphics[width=0.9\linewidth]{sections/7-battles/battlePhaseProcessDiagram.pdf}
    \caption{The conceptual order in which battle phases are be executed,
             ignoring turn boundaries and turn order.}
    \label{fig:battlePhaseProcessDiagram}
\end{figure}

An important reason for this order is that the \KEY{defender} is always
be able to retreat, after \KEYB{only} going through a \KEY{strike phase}.
Each subsequent turn that they wait, an additional \KEY{defence phase} then an
\KEY{attack phase} has also happened. Maintaining this order solves the
retreating problem, making the battle system \mbox{fair for all players}.

\subsection{\KEY{Scheduling} the different \KEY{types} of battle phase}

To maintain the order shown in \autoref{fig:battlePhaseProcessDiagram},
battle phases are \KEY{scheduled} to happen at different times
within turns. However, Rasc's \KEY{partially simultaneous} turn system
makes this complicated to do, as the correct \KEY{scheduling} order to do
this, depends on the order in which the two participating players in a
battle take their turns. This depends on whether each player is AI or human
and, in the case that they are both AI players, which one takes their turn
first. All of this results in five possible order combinations.
For context, the exact battle \KEY{scheduling} orders used in each of
these five combinations is shown in \autoref{fig:battleCompleteDiagram}.

\begin{figure}[ht]
    \centering
    \hspace*{-0.125\linewidth}\includegraphics[width=1.25\linewidth]{sections/7-battles/battleCompleteDiagram.pdf}
    \caption{Battle \emph{scheduling} orders for each of the \textbf{five}
             possible combinations of turn order.}
    \label{fig:battleCompleteDiagram}
\end{figure}

\subsection{Summary}

Alongside new \KEY{game mechanics} such as garrisons and sieges, the
new battle system in \KEY{Rasc version 2} causes the game to move
less quickly. It also presents users with additional options and decisions
to make, overall providing a more interesting experience.

However, as new \KEY{game mechanics} have been added, Rasc's AI players
must also be updated in order to make effective use of them. Rasc relies
on computer-controlled opponents to keep the game challenging, the process
of improving these is described in the next section.
