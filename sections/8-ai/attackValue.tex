
\subsection{Province desirability}\label{desirability}

For one player to \KEY{attack} another in Rasc, they need to do two things.
First, they need to place armies in appropriate \emph{edge provinces}, to
attack from. Since armies cannot be moved on the same turn
which they are placed, the player must wait until the next turn before they
can move those armies forward, to attack \KEY{enemy} provinces.

Figuring out \KEY{which} provinces to place armies in, to most effectively
\KEY{attack} other players, is a problem faced by the AI system. In
\KEY{Rasc version 1}, AI players approached this in a simple way,
using the idea of \KEY{province desirability}. This measures, for
adjacent enemy provinces, how useful it would be to take them.
This is based on a range of factors, such as whether taking the
province would progress the player closer to owning a complete area,
whether the province is owned by any player, and if so the relative
size of the player who owns it.

Each turn, AI players in \KEY{Rasc version 1} simply found the adjacent
enemy province with the largest \KEY{province desirability}, and placed
all available army units adjacent to it. The following turn, AI players
\KEY{always} attacked using all available armies. All of this caused another
\KEY{obvious weakness} in behaviour: AI players would often
attack \KEYB{exactly the same province} each turn, regardless of
the locations of surrounding players' armies. Human players could easily
exploit this, as they only needed to defend the single place which AI players
\KEY{always attacked} -- this was usually easy to predict.

Along with adding the ability for AI players to \KEY{defend} their land
(described earlier), \KEY{Rasc version 2} also makes improvements to
AI players' \KEY{attacking} behaviour. For example,
\KEYB{ongoing battles} and \KEYB{the locations of enemy armies}, are now
considered as part of \KEY{province desirability} (\AUTOPAGEREF{fig:ulsterMap}).

\KEY{Province desirability} is increased, for provinces in which the AI player
is losing a battle -- this allows AI players to reinforce battles,
even when they are in enemy territory.
Also, depending on AI \KEY{behaviour} (\KEY{aggressiveness} and
\KEY{cautiousness}), the presence of enemy armies in a province either
increase or decrease \KEY{province desirability}. This means that some
AI players actively try to \KEY{avoid} battles, while others actively
try to \KEY{start} battles.

Other improvements have also been made to how armies are placed and moved. To
place armies required for \KEY{attacking}, AI players now proportionally split
armies, between provinces adjacent to a small fraction of \KEY{most desirable}
enemy provinces, (this is similar to how \KEY{defensive} armies are placed,
described earlier). AI players now also avoid \KEY{attacking}, when
it would start a battle they cannot win.

\begin{figure}[ht]
    \centering
    \includegraphics[width=0.8\linewidth]{sections/8-ai/ulsterMap/ulsterValue.pdf}
    \caption{The perceived \KEY{province desirability}, for each enemy province
             adjacent to the yellow AI nation. Note especially that the
             lowest valued provinces are those where the player is
             \KEY{already winning} a battle or siege, so reinforcing would
             serve little purpose.}
    \label{fig:ulsterMap}
\end{figure}

\subsection{Summary}

The addition of varied \KEY{personalities}, alongside improved \KEY{attack} and
\KEY{defence} behaviour, now mean that AI players no longer have
many \KEY{obvious weaknesses}, and act in a more human-like way.
Despite Rasc's AI being implemented using a fairly simple \KEY{reactive}
approach, AI players in \KEY{Rasc version 2} make for challenging
and interesting opponents.
