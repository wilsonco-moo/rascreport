
\subsection{AI player personalities: \KEY{behaviour values}}\label{aiBehaviour}

A significant problem with AI players in \KEY{Rasc version 1}, was that
each AI player on the map carried out exactly the same process each
turn, with no variation. This caused all AI players on the map to act
in the same way, and make the same kinds of decisions, adding to their
predictability.

To improve on this, \KEY{Rasc version 2} aims to give AI players different
\KEY{personalities}, which control their tendency to act in certain ways.
For example, one AI player may act in a very \KEY{aggressive} way,
while another may act in a very \KEY{defensive} way.
To represent personalities, each AI player now has four \KEY{behaviour values}:
\KEY{aggressiveness}, \KEY{extravagance}, \KEY{focus} and \KEY{cautiousness}.
Each \KEY{behaviour value} controls a different part of AI players'
\mbox{decision-making} process, described in more detail later.

As is shown in \autoref{fig:behaviourDiagram}, each \emph{behaviour value} is
represented using an integer between 0 and 100 (inclusive). At the start
of the game, each of the four values are randomly chosen
(uniformly distributed) for each AI player, and not changed after that.
This way, each AI player has their own consistent personality, persisting
for the duration of the game.

Using the \KEY{player information summary}
(\autoref{fig:aiBehaviourDiplostatics}), human players are easily able to
view the personality of any AI player. This allows them to make informed
choices about strategy -- for example, a human player may want to be more
wary of an AI player shown as being \emph{very aggressive}.

\begin{figure}[ht]
    \centering
    \includegraphics[width=0.9\linewidth]{sections/8-ai/behaviourDiagram/behaviourDiagram.pdf}
    \caption{To make it more convenient to display \KEY{behaviour values},
             each one is split into six categories. For example,
             an AI player must have a (very low) \KEY{aggressiveness}
             value between \mbox{0 and 16} (inclusive), to be classed
             as \KEY{very defensive}.}
    \label{fig:behaviourDiagram}
    \vspace{-0.5cm}
\end{figure}

\begin{figure}[ht]
    \centering
    \includegraphics[width=0.7\linewidth]{sections/5-ui/finalDiplostaticsMenu.png}
    \caption{For each AI player, their four \KEY{behaviour values} are shown
             at the top-right of the \KEY{player information summary} (see
             \AUTOPAGEREF{universalPlayerButton}), using the categories
             described in \autoref{fig:behaviourDiagram}.}
    \label{fig:aiBehaviourDiplostatics}
\end{figure}

\subsubsection{Aggressiveness}
This controls how AI players balance their resources between attacking
and defending, and how they respond to \KEY{threat},
(\KEY{threat} is described in \AUTOPAGEREF{threat}). Depending on
\KEY{aggressiveness}, some players focus on \KEY{defending} their own
territory, while others focus on \KEY{attacking} the territory of others.

A \KEY{defensive} AI player will spend most of its resources placing
armies in attempt to \KEY{defend} its territory, and reduce \KEY{threat}.
\emph{Defensive} AI players also put much more focus on building
\KEY{garrisoned} armies and \KEY{forts}
(see \AUTOPAGEREF{garrisonsSieges}).

An \emph{aggressive} AI player will tend to largely ignore
\KEY{threat}, and spend most of its resources placing armies in the most
useful places to \KEY{attack} from. \KEY{Aggressive} AI players also
actively try to attack \KEYB{armies} in enemy provinces, rather
than trying to avoid battles like \KEY{defensive} players. They are
additionally much more driven towards gaining footholds in areas,
in attempt to harm other players' resource production.



\subsubsection{Extravagance}
This controls how AI players spend their resources -- how they are balanced
between placing \KEY{armies} and constructing \KEY{buildings}, (\KEY{buildings}
are described in \AUTOPAGEREF{resources}). In Rasc, the more armies that a
player currently owns, the more army maintenance (in currency) that they pay,
each turn.

A \KEY{conservative} AI player will try to have fewer
armies at once, in an attempt to try to accumulate currency for
constructing buildings. \KEY{Conservative} AI players rarely
go into \KEY{debt}, and due to constructing more buildings,
often do better later on in the game.

An \KEY{extravagant} AI player will place as many armies
as they can. This tends to result in them playing better during the
start of the game, but often running into \KEY{debt} due to
army maintenance (and thus not being able to place armies).
They also lose out later on, due to not spending resources on
constructing buildings.



\subsubsection{Focus}
This controls how the AI player spreads out its armies, in both army
placement and army movement.

An \KEY{unfocused} AI player will place a lot of small armies,
all around their borders, usually helping \KEY{defence} effectiveness. They
will also attack from lots of different provinces, using small armies.

A \KEY{focused} AI player will only place armies in a small fraction
of its provinces each turn. They will also attack only a small fraction
of the possible provinces at once, using larger armies, often helping
\KEY{attack} effectiveness. Focused AI players also prioritise attacking
and defending higher value provinces, often ignoring less valuable provinces.



\subsubsection{Cautiousness}
This controls how the AI player perceives battles and threat.

A \KEY{reckless} AI player will consider \KEY{threat} to be counteracted
by defensive armies, even when there are \KEYB{far fewer} defensive armies
than \KEYB{necessary}. They will also reinforce battles poorly, often not even
to the point where they start winning. This usually causes \KEY{reckless}
AI players to \KEYB{attack more often}, as they do not wait
until they have large enough armies before starting. This \KEY{can} help
them to expand quicker, but causes them \KEY{defend} land less
effectively.

A \KEY{cautious} AI player will place many more defensive armies
to counteract threat, and avoid battles unless they can definitely win them.
They will also reinforce battles much more readily,
often more than is necessary. This usually helps them to play
better especially when \KEY{defending}, as when an enemy is largely
outnumbered the other side will take fewer losses.
