
\subsection{Context: Reason for including \KEY{AI players}}

Rasc is a multiplayer game, which not only allows human players to compete,
but also provides computer controlled opponents: \KEY{AI players}. These
\KEY{AI players} are intended to \KEY{act like} human players, to make
Rasc more challenging and engaging.

The first major reason for including AI players in Rasc, is that it allows
much larger maps to be practical, without them seeming `empty'. Without
AI players, even with several human players, a Rasc game on
any reasonably sized map will mostly consist of claiming empty (unowned)
provinces. This can make the start of the game extremely dull.

Once AI players are introduced, human players have more options than
simply fighting each other and grappling over empty provinces.
What often happens, is that human players
team up with each other, unofficially forming `alliances' to defeat
AI players -- who are common enemies.

The inclusion of AI players also makes it practical to play Rasc
in \KEY{singleplayer} mode. This is where a human player can play
on their own, competing only against AI players. For this to be
enjoyable, the AI players must pose a good challenge, as otherwise
the game would be too easy -- potentially making the game boring.

An important point is that AI players must have as few
\KEY{obvious weaknesses} as possible. These are ways in which AI players
behave, which allow other players to \KEY{easily} win against them
just by doing specific actions. Once a human player learns of
\KEY{obvious weaknesses} in AI players, they are able to beat AI players
too easily -- again, potentially making the game boring.

\subsection{Information, strategies and approaches}

In the simplest sense, each player in Rasc has two main \KEY{objectives}:
claim as many provinces
as possible, and avoid losing provinces owned already. Rasc also
has various other \KEY{game mechanics}, such as armies, battles, resources
and areas. These mean that to complete the \KEY{objectives} effectively,
players must make decisions \KEY{strategically}. For human players this keeps
the game interesting, but complicates the implementation of AI players.
To remain competitive with humans, the AI system must attempt to
replicate human-like \KEY{strategy}.

Each turn, AI players must use \KEY{information} from the map, and make
\KEY{strategic} decisions as to which actions to take, in order to best
complete the \KEY{objectives}. To do this, many approaches such as
\KEY{machine learning}\cite{machineLearning} techniques could be used.
However, due to Rasc's large \KEY{scale}, the complexity of its
\KEY{game mechanics}, and the time constraints on AI players' turns,
the decision was made to use a simpler \KEY{reactive}\cite{reactive}
approach. This is where AI players have little internal state, and instead
of learning from past experience, make decisions based on current
\KEY{information}, using \KEY{pre-programmed} conditions.
This \KEY{reactive} approach is used in both \KEY{Rasc version 1} and
\KEY{Rasc version 2}, although in \KEY{Rasc version 2} the AI system
has been improved significantly, to play more competitively and make
effective use of the new \KEY{game mechanics}.

In Rasc, both human and AI players have access to \KEY{all} information
across the whole map. For human players, most of this is displayed in the
\KEY{list of player statistics} (\AUTOPAGEREF{universalPlayerButton})
and the map itself. This is in contrast
to many other strategy games which block players from knowing too much
about others. While human players in Rasc \KEY{can} use the overall map
for making \mbox{high-level} \KEY{strategic} decisions, most of the time
they will only look at the region of the map immediately around them.
This is similarly true
for AI players: for simplicity, AI players only use \KEY{local information}
from provinces and players around them to make decisions. 

\subsection{AI players in \KEY{Rasc version 1}}

In \KEY{Rasc version 1}, the behaviour of AI players is simple and limited.
Each turn, every AI player carries out the following process:
(Note: adjacent, edge and centre provinces are described in
\AUTOPAGEREF{fig:irelandNation}).
\begin{enumerate}
\item For each army in an edge province, move it into the most \KEY{desirable}
      adjacent province owned by an opposing player. This has the effect of
      attacking other players. The idea of \KEY{province desirability} is
      described in more detail later.
\item For each army in a centre province, move it to a random adjacent
      province. This avoids armies from getting \KEY{stuck} with nowhere
      to go.
\item Work out the number of armies that can be placed this turn,
      and place \KEYB{all of them} in the province which is adjacent
      to the most \KEY{desirable} enemy province. This has the effect
      of placing armies in a (fairly) useful place to attack \KEY{next} turn.
\end{enumerate}

\begin{figure}[ht]
    \vspace{-0.3cm}
    \centering
    \includegraphics[width=0.7\linewidth]{sections/8-ai/irelandNation/irelandNation-diagram.pdf}
    \caption{For the bright-green nation at the
             \mbox{bottom-left}, \KEY{edge provinces} (\KEYB{E})
             are those which are adjacent to another player, whereas
             \KEY{centre provinces} (\KEYB{C}) are those which are not.
             \KEY{Adjacent provinces} (\KEYB{A}) are those adjacent to the
             player's land, but owned by another player.}
    \label{fig:irelandNation}
    \vspace{-0.8cm}
\end{figure}

Considering how simple this process is, in \KEY{Rasc version 1} it works
suprisingly well. However, with the addition of various new
\KEY{game mechanics} in \KEY{Rasc version 2}, this simple AI system has
become increasingly inadequate, exposing multiple \KEY{obvious weaknesses}.

To rectify this, in \KEY{Rasc version 2} a significant number of improvements
have been made to the way in which AI players behave. These allow AI players
to be competitive with human players, even with Rasc's new
\KEY{game mechanics} and the continued use of a \KEY{reactive} approach.
The most important improvements to AI player behaviour are described
in more detail in the following sections.
