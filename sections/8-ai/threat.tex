
\subsection{Threat evaluation and defensive army placement}\label{threat}

In \KEY{Rasc version 1}, the largest \KEY{obvious weakness} in how AI players
behave is their lack of ability to \KEY{defend} their own land. AI players
only place armies in attempt to \KEY{attack}, and will never fight back if
another player attacks \KEY{their} land. Human players can easily exploit
this, by splitting their armies into many small chunks, to quickly take
large amounts of land from AI players.

To rectify this, \KEY{Rasc version 2} adds the idea of \KEY{threat} to
AI players. \KEY{Threat} is calculated in each province they own,
and is a measure of how likely that province is to be lost.
The intent is that \KEY{threat}, calculated in each province,
provides a good idea of \KEY{where} armies are most needed, for the purpose
of \KEY{defence}.

Depending on the \KEY{behaviour} (\KEY{aggressiveness}) of an AI player, they
allocate \mbox{40\% - 90\%} of the army units which they have available,
for placing armies \KEY{defensively}, based on \KEY{threat}. The
\KEY{threat} which an AI player perceives in a province they own, is
affected by the following factors:
\begin{itemize}
\item Enemy armies in adjacent provinces cause \KEY{positive threat}
      (proportional to army size), as these could \KEY{attack} next turn.
      For enemy players who are much larger by total count of owned provinces,
      and thus more powerful, \KEY{threat} from \KEY{their} armies is increased
      proportionally. Information about province count is known from the
      \KEY{statistics system} (\AUTOPAGEREF{statistics}).
\item Armies owned by the player cause \KEY{negative threat}
      (proportional to army size), as these can \KEY{defend} the
      province from \KEY{attacks}. The extent to which \KEY{threat}
      is counteracted depends on the AI player's \KEY{behaviour}
      (\KEY{cautiousness}).
\item A large amount of \KEY{positive threat} is perceived when an enemy
      army is besieging the province, or when two enemy players are
      engaged in a battle within the province.
\item When the AI player is involved in an ongoing battle within the province,
      \KEY{threat} is generated based on which side is winning the battle.
      \KEY{Threat} is positive when an enemy army is winning the battle,
      but negative when the AI player is winning.
      The point at which an AI player considers that they are \emph{winning}
      a battle, is decided by their \emph{behaviour} (\KEY{cautiousness}).
\end{itemize}
An example of \KEY{threat} values calculated for each province of a small AI
nation, is shown in \autoref{fig:threatMap1}.

\begin{figure}[ht]
    \centering
    \includegraphics[width=0.8\linewidth]{sections/8-ai/threatMap/threatMap1.pdf}
    \caption{This shows \emph{threat} perceived by the green AI player,
             in each of its provinces.
             \emph{Threat} in each province is perceived as higher
             in provinces with larger adjacent enemy armies. Also,
             where adjacent enemy armies are outnumbered by friendly
             \KEY{defensive} units, the \KEY{threat} is perceived as negative.
             For provinces with no nearby enemy armies, \KEY{threat} is
             perceived as zero.}
    \label{fig:threatMap1}
\end{figure}

The objective for AI players is to place \KEY{defensive} armies such that
\KEY{threat} is reduced to zero or less, since armies owned by \KEY{them}
reduce \KEY{threat}. To do this, AI players proportionally split armies
between a small fraction of their \KEY{most threatened} provinces --
the fraction of provinces picked depends on the AI player's \KEY{behaviour}
(\KEY{focus}).

In provinces where new armies cannot be placed, due to ongoing battles or
sieges from enemy armies, \KEY{defensive} armies are placed in adjacent
provinces. This allows these armies to be moved into the battle or siege,
to counteract the enemy army, next turn.

With all of this in place, \KEY{defence} is no longer an \KEY{obvious weakness}
for AI players. When, as a human player, you place armies adjacent to
AI players, they very often respond by placing \KEY{defensive} armies
in their own land. This adds an additional level of challenge to gameplay,
keeping players engaged.
